export default {
  namespaced: true,
  state: {
    aside: [],
    allmenu: [],
    asideCollapseToggle: true
  },
  actions: {},
  mutations: {
    allMenuSet (state, menu) {
      state.allmenu = menu
      state.aside = menu[0]
    },
    asideSet (state, menuid) {
      var selmenu = {}
      state.allmenu.forEach(m => {
        if (m.id === menuid) {
          selmenu = m
        }
      })
      state.aside = selmenu
    }
  }
}
