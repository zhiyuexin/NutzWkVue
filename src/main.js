// Vue
import Vue from 'vue'
import App from './App'
// 核心插件
import j2 from '@/plugin/d2admin'
// store
import store from '@/store/index'

// 菜单和路由设置
import router from './router'
import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'

import XEUtils from 'xe-utils'
import VXEUtils from 'vxe-utils'

import axios from '@/plugin/axios'
import util from '@/libs/util'

Vue.prototype.$util = util

Vue.use(Avue)
Vue.use(VXEUtils, XEUtils)
// 核心插件
Vue.use(j2)
Vue.use(axios)
new Vue({
  router,
  store,
  render: h => h(App),
  created () {},
  mounted () {
    // 获取并记录用户 UA
    this.$store.commit('j2/ua/get')
    // 初始化全屏监听
    this.$store.dispatch('j2/fullscreen/listen')
  }
}).$mount('#app')
